import ctypes
import itertools
import gc
import os
import signal

import analyses
import config
import report


class TimeOut(Exception):
    pass


def alarm_handler(signum, frame):
    raise TimeOut()

signal.signal(signal.SIGALRM, alarm_handler)

class Orchestrator:

    UNICORN_OPTIONS = [False, True]
    INIT_STATE_OPTIONS = ['blank', 'full']

    def __init__(self, options):
        self.options = options
        self.analyses = []
        self.reports = []

    def run(self):
        self._prepare()
        self._run()

    def _prepare(self):
        self.tasks = []

        for scfile in self.options.files:
            for mode in self._gen_modes():
                task = Task(scfile, unicorn=mode['unicorn'],
                        init_state=mode['init_state'])
                self.tasks.append(task)

    def _gen_modes(self):
        if not self.options.compare:
            return [{'unicorn': self.options.unicorn,
                    'init_state': self.options.state,
                    }]
        else:
            modes = []
            for unicorn, init_state in itertools.product(
                    Orchestrator.UNICORN_OPTIONS, Orchestrator.INIT_STATE_OPTIONS):
                modes.append({'unicorn': unicorn, 'init_state': init_state})
            return modes

    def _run(self):

        if self.options.report_inline:
            _reporter = report.InlineReporter()

        for task in self.tasks:
            analysis, result = task.run()

            if self.options.report_inline:
                _reporter.report_csv(analysis)
            else:
                self.analyses.append(analysis)
                self.reports.append(result)

            gc.collect()
        if not self.options.report_inline:
            self._report()

    def _report(self):
        reporter = report.Reporter(self.analyses)
        if self.options.csv:
            reporter.report_csv()
        else:
            reporter.report_table()


class Task:
    def __init__(self, shellcode, unicorn=False, init_state='blank'):
        self.shellcode = shellcode
        self.unicorn = unicorn
        self.init_state = init_state

    def run(self):
        analysis = analyses.Analysis(self.shellcode, unicorn=self.unicorn,
                init_state=self.init_state)
        result = None

        try:
            signal.alarm(config.ANALYSIS_TIMEOUT)  # Set the timeout alarm
            result = analysis.run()
            signal.alarm(0)  # Didn't timeout. Remove the alarm.
            analysis.status = 'COMPLETED'
        except ctypes.ArgumentError:
            pass
        except TimeOut:
            analysis.status = 'TIMEOUT'

        return analysis, result
