import angr
import socket
import struct
import time

import utils
import report


class Analysis:
    def __init__(self, shellcode, unicorn=False, init_state='blank'):
        self.shellcode = shellcode
        self.unicorn = unicorn
        self.init_state = init_state
        self.binary = None
        self.report = None
        self.status = None

    def _prepare(self):
        with open(self.shellcode, 'rb') as fd:
            sc = fd.read()
        self.binary = utils.build_shellcode_wrapper(sc)

    def run(self):

        self._prepare()
        report.SyscallAccumulator.reset()

        start = time.time()

        load_options = {
            'main_opts': {
                'backend': 'elf',
                'arch': 'x86',
            },
            'auto_load_libs': False,
        }

        project = angr.Project(self.binary,
                simos=angr.simos.linux.SimLinux,
                support_selfmodifying_code=True,
                load_options=load_options,
                )

        project.simos.syscall_library.add('socketcall', DebugSocketCall)

        if self.init_state == 'blank':
            state = project.factory.blank_state()
        elif self.init_state == 'full':
            state = project.factory.full_init_state()

        if self.unicorn:
            state = project.factory.blank_state(add_options=angr.options.unicorn)

        state.options.add(angr.options.INITIALIZE_ZERO_REGISTERS)
        state.options.update(angr.options.resilience)

        sm = project.factory.simulation_manager(state)
        sm.explore()

        end = time.time()
        elapsed_time = end - start

        return self._report(elapsed_time, sm)

    def _report(self, elapsed_time, simgr):
        sc_report = report.SyscallAccumulator.records()
        self.report = report.Report(elapsed_time, sc_report, simgr)


class DebugSocketCall(angr.SimProcedure):

    htons = angr.SIM_PROCEDURES['posix']['htons']
    htonl = angr.SIM_PROCEDURES['posix']['htonl']

    def run(self):

        eax_concrete = self.state.reg_concrete('eax')
        ebx_concrete = self.state.reg_concrete('ebx')
        print(("[+] Hooked syscall number: " + str(eax_concrete)))
        print(("[+] ... ebx: " + str(ebx_concrete)))

        if eax_concrete == 102 and ebx_concrete == 3:

            port_bv_inverted = self.state.stack_read(14, 2)
            port_bv = self.inline_call(DebugSocketCall.htons, port_bv_inverted).ret_expr
            port = self.state.solver.eval(port_bv)
            print("[*] Connect Back port identified as: " + str(port))

            host_bv_inverted = self.state.stack_read(16, 4)
            host_bv = self.inline_call(DebugSocketCall.htonl, host_bv_inverted).ret_expr
            host_int = self.state.solver.eval(host_bv)
            host = socket.inet_ntoa(struct.pack('!L', host_int))
            print("[*] Connect Back host identified as: " + str(host))

            scr = report.SyscallRecord(self.state, eax_concrete, host, port)
            report.SyscallAccumulator.add(scr)

        return 0
