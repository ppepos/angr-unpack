import csv

from tabulate import tabulate


class Report:
    def __init__(self, elapsed_time, syscalls, simgr):
        self.elapsed_time = elapsed_time
        self.syscall_report = syscalls
        self.simgr = simgr

    def syscall_identified(self):
        found_host = False
        found_port = False
        result = "NOK"

        for scall in self.syscall_report:
            if scall.host == "13.37.13.37":
                found_host = True
            if scall.port == 1337:
                found_port = True

        if found_host and found_port:
            result = "OK"
        elif found_host or found_port:
            result = "PART"

        return result


class ReportSuite:
    def __init__(self):
        self.reports = []


class SyscallRecord:
    def __init__(self, state, number, host, port):
        self.state = state
        self.number = number
        self.host = host
        self.port = port


class SyscallAccumulator:

    instance = None

    def __init__(self):
        if SyscallAccumulator.instance == None:
            SyscallAccumulator.instance = self
        self.records = []

    @staticmethod
    def reset():
        SyscallAccumulator.instance = None
        SyscallAccumulator()

    @staticmethod
    def add(record):
        SyscallAccumulator.instance.records.append(record)

    @staticmethod
    def records():
        return SyscallAccumulator.instance.records


class Reporter:
    def __init__(self, analyses):
        self.analyses = analyses

    def _gen_headers(self):
        headers = ["shellcode",
                    "unicorn",
                    "init state",
                    "simgr.f",
                    "simgr.e",
                    "simgr.u",
                    "syscalls found",
                    "elapsed time",
                    ]

        return headers

    def _gen_data_row(self, an):
        if an.report:
            res = [an.shellcode,
                   an.unicorn,
                   an.init_state,
                   len(an.report.simgr.found),
                   len(an.report.simgr.errored),
                   len(an.report.simgr.unsat),
                   an.report.syscall_identified(),
                   an.report.elapsed_time,
                   ]
        else:
            res = [an.shellcode,
                   an.unicorn,
                   an.init_state,
                   "N/A",
                   "N/A",
                   "N/A",
                   "FAILED",
                   "TIMEOUT",
                   ]
        return res

    def _gen_data(self):

        data = []

        for an in self.analyses:
            data.append(self._gen_data_row(an))

        return data

    def report_table(self):
        headers, data = self._gen_headers(), self._gen_data()
        print(tabulate(data, headers, tablefmt="simple"))

    def report_csv(self):
        headers, data = self._gen_headers(), self._gen_data()

        with open('results.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(headers)
            writer.writerows(data)

        print(','.join(headers))
        for row in data:
            print(','.join(map(str, row)))

class InlineReporter(Reporter):
    def __init__(self):
        self.started = False
        self.headers = super()._gen_headers()

    def report_csv(self, analysis):

        data = super()._gen_data_row(analysis)

        if not self.started:
            mode = 'w'
        else:
            mode = 'a'

        with open('results.csv', mode, newline='') as csvfile:
            writer = csv.writer(csvfile)
            if not self.started:
                writer.writerow(self.headers)
            writer.writerow(data)
            self.started = True
