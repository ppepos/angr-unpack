# ABC deprecation worning in a recursive dependency...
import argparse
import warnings

import runner


def parse_args():
    parser = argparse.ArgumentParser(description='Analyse some shellcodes')

    parser.add_argument('-f', '--files', type=str, nargs='*', help='List of shellcodes to analyse')
    parser.add_argument('-u', '--unicorn', action="store_true", help='Use the unicorn engine for concrete emulation', default=False)
    parser.add_argument('-s', '--state', choices=['blank', 'full'], help='Initial state configuration', default='blank')
    parser.add_argument('-c', '--compare', action="store_true", help='Compare analyses with product of options', default=False)
    parser.add_argument('-i', '--report_inline', action="store_true", help='Inline the reporting to save memory. WARNING: Only supports csv output', default=False)
    parser.add_argument('--csv', action="store_true", help='Output to results.csv', default=False)

    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    orchestrator = runner.Orchestrator(args)
    orchestrator.run()


if __name__ == '__main__':
    main()
