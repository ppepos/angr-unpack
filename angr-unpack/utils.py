import os

from binascii import hexlify, unhexlify

import config


def _construct_source(shellcode):

    template = """
#include <stdio.h>

char shellcode[] = "%s";

int main()
{
  (*(void  (*)()) shellcode)();
}
"""

    shellcode = hexlify(shellcode)
    escaped_shellcode = r"\x" + r"\x".join(
            shellcode[n : n+2].decode() for n in range(0, len(shellcode), 2))

    return template % escaped_shellcode


def _compile_source(source, x64=False):

    wrapped_c_path = os.path.join(config.WORKDIR, "shellcode_wrapped.c")
    with open(wrapped_c_path, "w") as fd:
        fd.write(source)

    gcc_cmd = "gcc {} -o {} -fno-stack-protector -z execstack -no-pie".format(
            wrapped_c_path, wrapped_c_path[:-2])

    if not x64:
        gcc_cmd += " -m32"

    os.system(gcc_cmd)
    return wrapped_c_path[:-2]


def build_shellcode_wrapper(sc):
    source = _construct_source(sc)
    return _compile_source(source, x64=False)
