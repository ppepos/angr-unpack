---
title: Analyse de *shellcodes packés* avec angr
subtitle: Proposition de projet
author: Philippe Pépos Petitclerc
institution: Université du Québec à Montréal
recipient: M. Jean Privat
date: 8 mai 2020
class: INF889A - Analyse de programmes pour la sécurité
toc: true
bibliography: projet.bib
---

# Problématique

Les outils de détection de logiciels malveillants classifient souvent les
logiciels soit comme étant sains ou malveillant. Dans le cas où le logiciel est
considéré malveillant, les outils fournissent peu d'information sur le
comportement malicieux. Dans le cas où l'on aimerait connaître certains détails
sur le comportement de ceux-ci, il est souvent nécessaire d'identifier
manuellement le *shellcode* dans le logiciel, de l'extraire puis d'analyser
manuellement le comportement de ce *shellcode*.

L'analyse du shellcode peut-être dans le but de connaître ce que le *shellcode*
tente de faire sur le système. Par exemple, tente-t-il de lire des fichiers ou
d'exfiltrer des informations? C'est comportements peuvent être associés à des
appels à des fonctions de librairies ou des appels systèmes particuliers. C'est
ce que les analystes chercheront.

Par contre, l'analyse de *shellcodes* est difficile pour deux raisons
principales: les *shellcodes* sont dangereux et ne devraient pas être exécutés,
puis les *shellcodes* sont souvents packés ou codés, ce qui complexifie
sérieusement l'analyse de ceux-ci puisqu'il faut extraire le shellcode de son
enveloppe.

# Notions préalables (TODO: Définir shellcode et packer et exécution symbolique)

## *Shellcodes*

On entend par shellcode, un petit programme, en langage machine, souvent écrit
à la main, dans le but de satisfaire certaines contraintes (tailles,
instructions permises, octets permis, etc.). Les shellcodes sont utilisés comme
charge utile (*payload*) lors de l'exploitation d'une vulnnérabilité.

## *Packeurs* et Encodeurs

Ils transforment des *shellcodes* afin d'accomplir certains buts. Par exemple,
évader des logiciels de sécurité, retirer des caractères interdis, chiffrer des
informations contenues dans le *shellcode*.

## Exécution Symbolique

Technique d'analyse de programme qui exécute le programme sur un domaine
abstrait. Plutôt que d'associer des valeurs aux cases mémoires, on y associe
des arborescences de contraintes (prédicats) qui expriment les valeurs
possibles de ces cases mémoires en relation avec les autres et la progression
du programme. Un des intérêts de l'exécution symbolique est de pouvoir résoudre
ces contraintes (par un solveur SMT) afin d'obtenir des valeurs possibles selon
le chemin d'exécution du programme.

# Objectifs du projet

objectifs visés par la présente recherche sont les suivants:

 * Faciliter et accélérer l'analyse de *shellcodes* packés en automatisant une
   partie de la tâche.
 * Permettre de trier ou d'identifier rapidement certains comportements des
   shellcodes
 * Ne pas recourrir à l'exécution (et donc à l'analyse dynamique) du shellcode

# Stratégies et implémentation

## Dépôt de code

L'implémentation discuté dans cette section est disponible sur le dépôt de code Gitlab [@pepos_petitclerc_angr-unpack_2020].

## Un cadriciel d'analyse de programmes binaires et un exécuteur symbolique

Les *shellcodes* étant des petits programmes binaires, le moteur d'analyse
choisi doit pouvoir travailler directement avec le code binaire du programme.
Le choix d'un moteur d'exécution symbolique paraît logique parce que l'analyse
de shellcode semble un sujet pertinent pour l'analyse symbolique. Les
shellcodes sont généralements petits et relativement linéaires (dépourvus de
boucles complexes).

La stratégie de base qui est proposée pour atteindre ces objectifs est
d'utiliser un cadriciel d'analyse de programmes binaires et d'abuser de
fonctionalités d'exécution symbolique et des analyses additionnelles fournises
pour étudier le comportement des *shellcodes*. On cherchait également à pouvoir
introduire des court-circuits (*hooks*) sur certaines instructions. Finalement,
nous nécessitions des capacités de construction de graphes de flot de contrôle
à partir du programme binaire. Parmi les outils d'analyse de programmes binaires, `angr` [@angrangr_2020]
paraît être le plus complet et mature à ce jour. C'est donc ce qui justifie son
choix comme cadriciel d'analyse pour ce projet.

## Identification d'appels systèmes

Afin d'identifier les appels système potentiels des *shellcodes* analysés, on a
exploité la possibilité de court-circuiter ces appels lors de l'exécution
symbolique.  En accrochant une procédure symbolique avant chaque appel système,
il nous est possible d'inspecter les contraintes recueillies puis de résoudre
les valeurs possibles des registres et des paramètres à l'appel.  Ceci nous
permet d'obtenir le numéro de l'appel système ainsi que la valeur ses arguments
à l'entrée de l'appel. Dans le prototype, nous avons cherché à identifier les
appels à `socketcall` puis à déterminer s'il s'agissait d'une connexion vers un
port distant. Le cas échant, nous résolvons les valeurs possibles des
paramètres, ce qui donne les couples (IP, Port) de destination de la connexion
tentée. Voici donc un exemple de *hook* qui permet de résoudre l'adresse IP de
destination et le port associé.

``` python
class DebugSocketCall(angr.SimProcedure):
	...
    def run(self):
        eax_concrete = self.state.reg_concrete('eax')
        ebx_concrete = self.state.reg_concrete('ebx')

		# Verify we are doing a `socketcall` (eax == 102)
		# and we are doing a `connect` (ebx == 3)
        if eax_concrete == 102 and ebx_concrete == 3:
			# Find a solution for destination address
            host_bv_inverted = self.state.stack_read(16, 4)
            host_bv = self.inline_call(DebugSocketCall.htonl, host_bv_inverted).ret_expr
            host_int = self.state.solver.eval(host_bv)
            host = socket.inet_ntoa(struct.pack('!L', host_int))

			# Find a solution for destination port
            port_bv_inverted = self.state.stack_read(14, 2)
            port_bv = self.inline_call(DebugSocketCall.htons, port_bv_inverted).ret_expr
            port = self.state.solver.eval(port_bv)
			...
```

## Extraction de *shellcodes* *dépackés*

La stratégie identifiée pour extraire les *shellcodes* *dépackés* se basait sur
la construction initale d'un graphe de flot de contrôle puis l'identification
de l'exécution d'un bloc qui ne se retrouvait pas dans le graphe de flot de
contrôle.  Ce premier bloc serait, selon notre hypothèse, le point d'entrée du
shellcode sous-jacent.

Malheureusement, cette capacité a dut être abandonné du projet suite à
l'exploration initiale de la stratégie. Dans les différentes phases de lifting
de blocs et d'exécution symbolique, `angr` relocalise très fréquemment les
blocs en mémoire et utilise une représentation interne qu'il n'expose pas
directement pour déterminer l'adresse des blocs. Ceci est dut aux différents
contextes d'exécution qui sont possible dans `angr` comme *Unicorn* (le moteur
d'exécution concrète), les procédures simulées écrites en *Python*, les
instructions binaires initiales, et les instrucstions liftés (en *VEX*). Nous
avons donc choisi d'exclure du cadre de la présente recherche cette capacité
d'extraction de *shellcodes*.

## Explorer `Symbion`

`Symbion` une nouvelle technique d'exploration introduite dans `angr` pour
attaquer le problème des *packeurs* comme `UPX` qui complexifient beaucoup
l'analyse d'un programme [@degrigis_symbion_nodate]. La technique repose sur l'exécution concrète non
émulée du programme jusqu'à l'atteinte d'un certain point où l'exécution est
poursuivie symboliquement. Nous espérions être en mesure d'exploiter cette technique d'exécution pour compléter les cas où l'exécution symbolique dynamique ne serait pas en mesure d'explorer l'espace d'états ou de trouver une solution dans un temps raisonnable.

Malheureusement, `Symbion` est encore jeune et était trop instable pour être
exploitable pour étendre les capacités de notre outil. De plus, son API
d'intéraction avec la progression de l'exécution étant plus restreint que celui
de l'exécution symbolique, il ne nous était pas possible de chercher à
identifier l'exécution de nouveaux blocs inconnus des GFC initiaux. Ultimement,
nous avons choisis d'exclure `Symbion` de la présente étude.

## Variations et comparaisons

### Choix du format

`angr` présente la capacité d'analyser plusieurs formats d'exécutables dont
deux sont pertinents dans le cadre du projet explorateur proposé. Il est
possible de charger et d'analyser un programme au format *ELF* (*Executable and
Linking Format*). `angr` supposera donc qu'il peut faire confiance aux
artéfacts et aux metadonnées du format pour augmenter son analyse. À l'opposé,
on peut également charger un exécutable de format *Blob* qui représente
simplement des instructions sans enveloppe de format exécutables.

Bien que ce soit le format *Blob* qui représente réellement la situation des
shellcodes, nous avons constatés que dans l'état actuel du chargeur de
programmes pour `angr`, *CLE* (*CLE Loads Everything*), il était impossible
d'analyser un *Blob* qui ferait des appels systèmes. C'est au niveau de
l'intégration de l'ABI du système d'exploitation avec l'architecture de *Blob*
que les limites apparaîssaient. Nous avons donc également choisis de ne
considérer que l'architecture *ELF*. Nous avons donc fait le travail
d'envelopper chaque shellcode dans un ELF qui ne faisait qu'appeler le
*shellcode* à analyser.

### L'utilité de l'exécution concrète partielle

Dut aux particuliarités des *shellcodes packés*, nous supposions que
l'exécution concrète de certains blocs lors de son exécution symbolique soit
profitable aux résultats. Puisque certaines instructions sont approximées dans
leur représentation symbolique pour réduire la complexité de l'analyse et
puisque les *packeurs* abusent souvent d'instructions plus rarement utilisées
(et donc plus rarement implémentées dans les outils d'analyse) afin de déjouer
certaines analyses, pouvoir exécuter concrètement certaines instructions ou
blocs pourrait s'avérer avantageux. Aussi, l'exécution concrète est
considérablement plus rapide que l'exécution symbolique et est moins sujete à
l'explosion combinatoire de l'exécution symbolique.

Dans le monde d'`angr`, l'exécution concrète de certains blocs est possible au
travers de l'engin d'émulation `Unicorn` qui est exposé comme une technique
d'exécution additionnelle dans l'API d'`angr`. Nous avons donc choisis de
comparer les résultats avec et sans avoir recours à l'exécution concrète (via
`Unicorn`) lors de l'exécution symbolique dynamique du programme. Les résultats
seront discutés dans la section d'[analyse des résultats](#analyse-des-résultats).

### Initialisation de la mémoire

Une autre particuliarité des *shellcodes* est le fait qu'ils sont conçus pour
avoir le même comportement peu importe leur position en mémoire et peu importe
l'état actuel de la mémoire et des registres. Ce sont ces particuliarités qui
nous ont poussés à nous intéresser à l'impacte de l'état initial de la mémoire
et des registres au début de l'exécution. Cet état est possible à manipuler
dans `angr` en injectant un état initial arbitraire à l'exécution symbolique.
On peut donc construire un état simple qui ne contient pas de contraintes sur
l'état de la mémoire et des registres et un autre dans lequel on contraint les
registres à contenir des `0` au tout début de l'exécution. Nous avons donc
comparé l'impact de l'initialisation à zéro de la mémoire (et des registres)
sur les résultats d'identification des appels système.  Nous nous attendions à
ce que ce changement ait peu d'éffets sachant que les shellcodes analysés sont
construits pour minimiser l'impact de l'état de la mémoire au début de leur
exécution. Les résultats seront discutés dans la section
d'[analyse des résultats](#analyse-des-résultats).

# Analyse des résultats

Afin d'évaluer l'impacte des différentes variations de stratégie, nous avons
comparés les résultats obtenus pour chaque combinaison des éléments suivants:
*packeur*, appel ou non à l'exécution concrète d'`Unicorn`, initialisation à
zéro ou non de la mémoire et des registres. Pour ce faire, nous avons utilisés
`msfvenom`, un composant du *Metasploit Framework* qui permet de générer des
shellcodes et de les coder ou les *packer* de différentes façons. Tous les
*shellcodes* ont étés générés de façon identique à l'exception du *packer*
choisi.

Pour établir si une analyse de *shellcode* a réussie, on identifie tout d'abord
tous les appels systèmes potentielles du *shellcode*. On vérifie par la suite
qu'il y a eu au moins un appel à `socketcall` identifié avec le mode `connect` vers le
couple (IP, Port) choisi lors de la génération des *shellcodes*.

Le temps maximal alloué à l'analyse était de dix minutes par *shellcode*.

*Un tableau est disponible [en annexe](#annexe) avec l'ensemble des résultats obtenus.*

La première observation importante est le fait que lorsque l'analyse
réussissait pour un *shellcode* en particulier, elle réussissait pour tous ses
variations. C'est en ligne avec l'hypothèse originale que l'impact de l'état
initial de la mémoire et des registres au début de l'analyse est peu
importante. Ce résultat montre aussi que l'exécution concrète n'a pas permi
d'augmenter le nombre de cas d'analyses réussies.

Par contre, on remarque que certains *shellcodes packés* ont été plus rapides à
analyser lorsque l'on exploitait le moteur d'exécution concrète pour certains
blocs. Pour d'autres *shellcodes*, le gain de performance est négligeable. Il
est possibles que ces *shellcodes* soient si simples qu'il n'y avait pas de
blocs candidats à l'exécution concrète.

En somme, même si la stratégie n'a permis que de d'identifier les destinations
des connexions que de sept *shellcodes* sur 19, environs 37%, dans ces cas, il
est possible d'identifier ces informations en quelques secondes de manière
complètement automatique.

# Conclusion

La stratégie explorée permet donc l'identification automatique de certains
comportements d'un tier des *shellcodes* analysés. Ultimement, c'est un gain de
temps considérable par rapport au travail qui est requis pour extraire cette
même information d'une analyse manuelle.

Pour améliorer ces résultats, il est envisageable de chercher à identifier la
différence entre les *shellcodes* où l'analyse réussit et où elle échoue. Il
est possible que cette différence soit structurelle comme il est possible
qu'elle soit au niveau de l'implémentation de certaines instructions utilisées.
Aussi, creuser dans la représentation interne des adresses des blocs
permettrait certainement de progresser dans la stratégie d'extraction des
*shellcodes* *dépackés*.

\newpage
# Annexe

| Packeur/Encodeur         | `Unicorn` | État initial | Erreurs |  Tests  | Temps (s)|
|--------------------------|---------|------------|---------|--------|----------|
| aucun                    | Non     | Zéro       | 0       | OK     | 0.90917  |
| aucun                    | Non     | Défaut     | 0       | OK     | 0.61316  |
| aucun                    | Oui     | Zéro       | 0       | OK     | 0.65837  |
| aucun                    | Oui     | Défaut     | 0       | OK     | 0.65551  |
| alpha_mixed              | Non     | Zéro       | 0       | NOK    | 0.47717  |
| alpha_mixed              | Non     | Défaut     | 0       | NOK    | 0.46199  |
| alpha_mixed              | Oui     | Zéro       | 0       | NOK    | 0.51239  |
| alpha_mixed              | Oui     | Défaut     | 0       | NOK    | 0.53651  |
| alpha_upper              | Non     | Zéro       | 0       | NOK    | 1.54787  |
| alpha_upper              | Non     | Défaut     | 0       | NOK    | 0.76053  |
| alpha_upper              | Oui     | Zéro       | 0       | NOK    | 1.51089  |
| alpha_upper              | Oui     | Défaut     | 0       | NOK    | 1.53717  |
| avoid_underscore_tolower | Non     | Zéro       | 0       | NOK    | 0.51714  |
| avoid_underscore_tolower | Non     | Défaut     | 0       | NOK    | 0.50311  |
| avoid_underscore_tolower | Oui     | Zéro       | 0       | NOK    | 0.57166  |
| avoid_underscore_tolower | Oui     | Défaut     | 0       | NOK    | 0.57164  |
| bloxor                   | Non     | Zéro       | 0       | NOK    | 1.39805  |
| bloxor                   | Non     | Défaut     | 0       | NOK    | 1.34885  |
| bloxor                   | Oui     | Zéro       | 0       | NOK    | 1.42807  |
| bloxor                   | Oui     | Défaut     | 0       | NOK    | 1.44880  |
| call4_dword_xor          | Non     | Zéro       | 0       | OK     | 1.31607  |
| call4_dword_xor          | Non     | Défaut     | 0       | OK     | 1.31979  |
| call4_dword_xor          | Oui     | Zéro       | 0       | OK     | 1.37911  |
| call4_dword_xor          | Oui     | Défaut     | 0       | OK     | 1.38911  |
| context_cpuid            | Non     | Zéro       | 0       | NOK    | 0.81531  |
| context_cpuid            | Non     | Défaut     | 0       | NOK    | 0.79086  |
| context_cpuid            | Oui     | Zéro       | 0       | NOK    | 0.85959  |
| context_cpuid            | Oui     | Défaut     | 0       | NOK    | 0.85851  |
| context_stat             | Non     | Zéro       | 0       | NOK    | 1.40715  |
| context_stat             | Non     | Défaut     | 0       | NOK    | 0.81192  |
| context_stat             | Oui     | Zéro       | 0       | NOK    | 1.51553  |
| context_stat             | Oui     | Défaut     | 0       | NOK    | 1.54556  |
| context_time             | Non     | Zéro       | 0       | NOK    | 0.90104  |
| context_time             | Non     | Défaut     | 0       | NOK    | 0.88503  |
| context_time             | Oui     | Zéro       | 0       | NOK    | 0.95895  |
| context_time             | Oui     | Défaut     | 0       | NOK    | 0.96083  |
| countdown                | Non     | Zéro       | 0       | OK     | 4.50114  |
| countdown                | Non     | Défaut     | 0       | OK     | 5.61298  |
| countdown                | Oui     | Zéro       | 0       | OK     | 3.41347  |
| countdown                | Oui     | Défaut     | 0       | OK     | 3.41824  |
| fnstenv_mov              | Non     | Zéro       | 1       | NOK    | 0.94960  |
| fnstenv_mov              | Non     | Défaut     | 1       | NOK    | 0.85995  |
| fnstenv_mov              | Oui     | Zéro       | 1       | NOK    | 0.00892  |
| fnstenv_mov              | Oui     | Défaut     | 1       | NOK    | 0.00791  |
| jmp_call_additive        | Non     | Zéro       | 0       | OK     | 1.05317  |
| jmp_call_additive        | Non     | Défaut     | 0       | OK     | 1.01988  |
| jmp_call_additive        | Oui     | Zéro       | 0       | OK     | 1.07835  |
| jmp_call_additive        | Oui     | Défaut     | 0       | OK     | 1.08769  |
| nonalpha                 | Non     | Zéro       | 0       | OK     | 1.28665  |
| nonalpha                 | Non     | Défaut     | 0       | OK     | 1.23869  |
| nonalpha                 | Oui     | Zéro       | 0       | OK     | 1.40011  |
| nonalpha                 | Oui     | Défaut     | 0       | OK     | 1.40120  |
| nonupper                 | Non     | Zéro       | 0       | OK     | 0.61389  |
| nonupper                 | Non     | Défaut     | 0       | OK     | 0.61707  |
| nonupper                 | Oui     | Zéro       | 0       | OK     | 0.68081  |
| nonupper                 | Oui     | Défaut     | 0       | OK     | 0.68375  |
| opt_sub                  | Non     | Zéro       | 0       | NOK    | 0.53465  |
| opt_sub                  | Non     | Défaut     | 0       | NOK    | 0.51793  |
| opt_sub                  | Oui     | Zéro       | 0       | NOK    | 0.59706  |
| opt_sub                  | Oui     | Défaut     | 0       | NOK    | 0.59833  |
| service                  | Non     | Zéro       | N/A     | FAILED | TIMEOUT  |
| service                  | Non     | Défaut     | N/A     | FAILED | TIMEOUT  |
| service                  | Oui     | Zéro       | N/A     | FAILED | TIMEOUT  |
| service                  | Oui     | Défaut     | N/A     | FAILED | TIMEOUT  |
| shikata_ga_nai           | Non     | Zéro       | 1       | NOK    | 0.80037  |
| shikata_ga_nai           | Non     | Défaut     | 1       | NOK    | 0.69264  |
| shikata_ga_nai           | Oui     | Zéro       | 1       | NOK    | 0.84082  |
| shikata_ga_nai           | Oui     | Défaut     | 1       | NOK    | 0.84290  |
| single_static_bit        | Non     | Zéro       | N/A     | FAILED | TIMEOUT  |
| single_static_bit        | Non     | Défaut     | N/A     | FAILED | TIMEOUT  |
| single_static_bit        | Oui     | Zéro       | N/A     | FAILED | TIMEOUT  |
| single_static_bit        | Oui     | Défaut     | N/A     | FAILED | TIMEOUT  |
| xor_dynamic              | Non     | Zéro       | 0       | OK     | 3.66818  |
| xor_dynamic              | Non     | Défaut     | 0       | OK     | 3.64710  |
| xor_dynamic              | Oui     | Zéro       | 0       | OK     | 1.37699  |
| xor_dynamic              | Oui     | Défaut     | 0       | OK     | 1.38158  |

\newpage
# Bibliographie
